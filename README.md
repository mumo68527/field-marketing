## FM Managers
- WW Field Marketing Director: Leslie Blanchard @lblanchard 
- Manager Americas: Ken Grabowski @kengrabowski
- Manager APAC: Peter Huynh @phuynh
- Manager EMEA: Kristine Setschin	@ksetschin


## FM DRI per Region
**AMER**
- AMER East NE & SE:	Ginny Reib	@GReib
- AMER East Canada and LATAM: Ginny Reib @GReib
- AMER East-Central:	Rachel Hill  @rachel_hill
- AMER Named: TBH
- AMER West-PacNorWest:	TBH
- AMER Canada West: TBH
- AMER West - NorCal/SoCal/Rockies:	Rachel Hill	@rachel_hill
- AMER Public Sector - DOD and NSP - IC, Public Safety and Scientific agencies:	Helen Ortel	@Hortel 
- AMER Public Sector - SLED and Civilian - Health, Financial agencies, and PubSec Distributor Carahsoft: Kira Aubrey @kaubrey

**APAC**
- APAC:	Pete Huynh	@Phuynh
- Japan - TBH

**EMEA**
- EMEA Southern Europe & MEA: Juliette Francon   @juliette.frcn
- EMEA Northern Europe & UK/I & Russia:	Elena (Lena) Sheveleva  @Lena333
- EMEA Central Europe & CEE:	Sarina Kraft 	@sarinakraft

## FM Coordinators
- AMER NE/SE/PubSec: Katie Rogel @krogel
- AMER West: TBH 
- APAC: Timothy Tran @ttran5
- EMEA: Helena Dixon @Helenadixon 

**Notable sources of information:**
- [Field Marketing Handbook](https://about.gitlab.com/handbook/marketing/field-marketing/)
- [Slack rooms we use](https://about.gitlab.com/handbook/marketing/field-marketing/#slack-rooms-we-use)
