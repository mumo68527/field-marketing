This template can be utilized by the FMM to request a venue search from the FMC. Please fill out as much detail below as possible.   

## General

**FMM Owner:**  
**Event Name:**    
**Event Description:**  
**Date(s):**  
**Event Times:**  
**Max Headcount:**  
**Location (City/State):**  
**Venue(s) to be Included in Search:**  
**Venue Budget (Room Rental/F&B/AV):**  
**Will event be filmed?**

**Reminder to FMC to ask all venues if they require any PII from attendees (example - attendee list for security check-in).**

## Event Space and AV Specs

Please include all event space required (meeting rooms, reception area, registration area, etc.) as well as all setup and AV requests that would impact space/budget.

* Event Space 1  
  * Setup:  
  * Max Headcount:  
  * AV:  

## F&B

Please specify F&B requirements (beverage station during meeting, happy hour, breakfast, lunch, dinner, etc.).

## Agenda

Please provide a basic agenda if multiple types of events will be occurring (example: GitLab Connect to include meeting start/end time and happy hour start/end time).

## Additional Information

Please specify any requirements or additional information that might be helpful in narrowing your search (if you have any venues already in mind, specific areas in a city you want to stay within, unique or more traditional venues, indoor/outdoor, etc.).

## Assign


`Assign to your regional FMC`   
/label ~"Field Marketing" ~"mktg-status::plan"

