<!-- Purpose of this issue: To write copy for virtual events. -->

## FMC To Do
* [ ] Copy doc template cloned, shared and linked in epic
* [ ] Name this issue `Write Copy: <campaign name>` (ex. Write Copy: Security Workshop)
* [ ] Set due dates below based on [timeline guidelines](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) - note these are recommendations, up for discussion in collaborating with the team
* [ ] Link copy doc in Copy Doc section below
* [ ] Follow [copy deadline process](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-event-copy-deadline-process)
* [ ] When all copy is complete and Request Target List Issue or DB1 List is submitted, close issue

## Copy Doc 
*FMC to link copy doc here for easy access*  

## FMM To Do
* [ ] Complete the [Request Target List](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-confirm-target-list) issue (15 day SLA) and link to your epic - Due Due `FMC to add due date`
* [ ] [DB1 list creation and upload to Marketo](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#pushing-demandbase-lists-to-marketo) - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for landing page in copy doc - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for sales nominated invite in copy doc - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for email Invite 1 in copy doc - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for email Invite 2 in copy doc - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for email Reminder in copy doc - when complete, ping your FMC - Due `FMC to add due date`
* [ ] Write copy for email Follow Up in copy doc - when complete, ping your FMC - Due `FMC to add due date`

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"Field Marketing"
-->
