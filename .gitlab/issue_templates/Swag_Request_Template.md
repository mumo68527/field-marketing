## Swag Item Request
*This section to be filled in by the FMM requesting the swag. Please duplicate the below section for each swag item being sourced.*

- What swag item are you looking to source (t-shirts, mugs, etc.)?
- What is the all-inclusive budget per item?
- What is the total quantity needed?
- If sizing is needed for the item, please list the sizes and quantities per size here:
- What color(s) would you like sourced?
- Are there any defining features needed or links to what you have already found as options?
- Please list your Allocadia line item ID here: 
- Will this item be stored at the warehouse or shipped directly to someone?
   - If being shipped to someone directly, please provide their name/address/phone/email and make this issue confidential:
- What is the logo to be utilized (link logo file here):
    - Where is logo to be placed on the item?
    - Is other content requested, in addition to the logo (example - statement on back of shirt)?

## Item Details
*FMC to fill in below. Please duplicate the below section for each swag item being sourced.*

- Doc with sourced swag selections:
  - [ ] Confirm which option has been selected here:
- Production time of item:
- Confirmation from vendor (to be approved by FMM before processing):
  - [ ] FMM to check here once approved
- Proof from vendor (to be approved by FMM before processing):
  - [ ] FMM to check here once approved

## Shipping/Receiving
*FMC to track here. Please duplicate the below section for each swag item.*

- [ ] Item has been shipped (add tracking here):
- [ ] Item has been received by the warehouse (and is on the portal ready for ordering)

## Finance Details
*FMC to fill in below. Please duplicate the below section for each swag item being sourced if item charges are being tracked separately.*

- [ ]  Allocadia ID (line item ID used for tactic charges):
   - [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
- [ ]  Coupa requisition for order created by FMC - `Link Coupa req here if applicable for orders $10k+`
- [ ]  Coupa req approved
- [ ]  Invoice received
- [ ]  Invoice paid    

## Assign
`Assign to your regional FMC`  
/label ~"Field Marketing" ~"mktg-status::plan"
