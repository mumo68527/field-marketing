## :notepad_spiral: Event Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **FMM/Event Owner:** 
* **FMC:** 
* **Tactic/Event Type:** [**Webcast**](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#webcast)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Official Webcast Name:** 
* **Date:** `NOTE: Please make sure your webcast date is final and confirmed. Moving a webcast date is extremely time-intensive once setup is complete.`
* **Location:** Virtual 
* **Start/Stop Time and Time Zone for Event:** `NOTE: Please make sure your webcast time is final and confirmed. Moving a webcast time is extremely time-intensive once setup is complete.`
* **Landing Page:**
* **Allocadia ID (line item ID used for tactic charges):** (to be filled in by the FMC)
   * [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
* **Campaign Name (Marketo/SFDC):** (to be filled in by the FMC)
* **SAO Goal:**
* **Budgeted Costs (sponsorship + auxiliary cost):** 
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## Splitting Tactics
*This section is to be filled in if you are splitting your tactic between sub-regions. Please make sure each activity plan's line item panels match and are completed before submitting a contract request to your FMC.*
* **Lead FMM:** (please specify the FMM owner for SFDC here)
* **Allocadia IDs:** (please list the Allocadia category or sub-category IDs in each activity plan here for your FMC to reference)
* **Budget Breakdown:** (please list all sub-regions participating and what cost is allocated for each sub-region)

## :sleuth_or_spy: Description of Event
(Description)  

## :vertical_traffic_light: User Journey
(FMM to provide a description of the user journey)

## :paperclip: Planning & Recap Spreadsheet and Important Links
* [**Field Marketing Spreadsheet**](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1)  
Make a copy of the Field Marketing Spreadsheet and save here. NOTE: Be sure the document can be edited by anyone at GitLab. 
* [**Field Marketing Webcast and Workshop Handbook**](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/)
* [**SLA Workback Schedule**](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)

## :white_check_mark: FMC Request Checklist  
* Will leads be provided for upload? `YES/NO`
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists
* Will this event require sales-nominated workflow? - YES
* Will you need a Marketo landing page set up? YES
  * If YES, please specify the landing page requested go live date if different than what is specified in the SLA workback schedule: `Date`
  * If YES, please specify if your LP requires the below (copy must be provided in the copy doc)
    - [ ] Thank you/confirmation page
    - [ ] Confirmation email
    - [ ] Add to calendar feature in confirmation email
* Please specify email send times/time zones (send dates are pre-determined in [SLA workback schedule](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)):
  * [ ] Sales Nominated: `First Send Date/Time/Time Zone`
  * [ ] Invite 1: `Time/Time Zone`
  * [ ] Invite 2: `Time/Time Zone`
  * [ ] Reminder: `Time/Time Zone`
  * [ ] Follow up: `Time/Time Zone`
    * Do you want to send different follow up emails to different audiences? `YES/NO` (If YES, you must provide copy for the different emails in your copy doc and have specified audiences for each email send)
* If sending invites, will you be utilizing a [DB1 list](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#pushing-demandbase-lists-to-marketo) or require CM support for a target list (15 business day SLA)? `DB1/CM Target List`
* Add to the [GitLab Events Page](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)? `YES/NO`
   * If YES please link or add appropriate event description/summary of event to be added to the events page here: `Event description`  

## :pencil2: Zoom License Request
Once all of your speakers are confirmed and assigned, fill out and submit a [Zoom License Request](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=request_zoom_license_date) issue to your regional FMC within the appropriate [SLA timeframe](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280) and relate the request to this issue. 

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the FMM is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate to this issue under related issues. 

## :two_women_holding_hands: Channel Partner Section 
This section must be filled out if you are running this campaign with a channel partner. 

- [ ] Please add the name of the channel partner to the title of this issue. 
- [ ] Is the partner speaking? YES/NO    
- [ ] Is the partner driving registration? YES/NO   
- [ ] If the partner is driving registration, you must provide them with a unique UTM. 
   - [ ] Please add the unique UTM here: example: https://page.gitlab.com/pies-in-july-virtual-pizza-registration-page.html?utm_medium=email&utm_source=wwt&utm_campaign=piesinjulyfy22q2&utm_content=partnerlandingpage
   - [ ] Please add the SFDC UTM tracker: [example](https://gitlab.my.salesforce.com/00O4M000004oMS7). 
- [ ] Is the partner standing up the landing page for registration?  YES/NO   
- [ ] Is the partner supplying graphics? YES/NO  
   - [ ] If yes, have these graphics been approved by the design team? YES/NO    
- [ ] In Person Event? YES/NO
   - [ ] Are we in a Partner's Booth or Pavillion: YES/NO
      - [ ] If yes, which one? BOOTH/PAVILLION
   - [ ] Do we set up our own booth: YES/NO
   - [ ] Is our participation at a kiosk: YES/NO

### Lead follow-up with Channel Partner 
- [ ] Will the partner do initial lead follow-up? YES/NO   
   - [ ] If yes, will GitLab receive leads at any point?   
   - [ ] Please describe how the partner plans to follow-up on leads received from this event/campaign:    
- [ ] Please add any additional, relevant notes regarding the partnership on this event/campaign here: 

## :mailbox_closed: Campaign Outreach Details 
- [ ] Sales-Nominated
    - SALs/ISRs/SDRs have the ability to nominate contacts in SFDC to receive email invitations sent by Marketing. Inviting attendees is a multi-touch approach, so we strongly encourage following up with those who've been nominated to encourage them to register. You can check their registration status in the SFDC campaign. Refresher on sales-nominated process [here](https://about.gitlab.com/handbook/marketing/events/#for-field-events). 
- [ ] SAL/ISR/SDR Outreach
- [ ] Marketing Outreach
- [ ] Paid Advertising
- [ ] Social Media
- [ ] Third-Party (example: Banzai.io)

## :construction_site: Prepare
* [ ] Attendee List: Please link attendee list tab here. NOTE: This should be added as a tab on the planning and recap spreadsheet and should not be in a separate document.
* [ ] Once [these items](https://about.gitlab.com/handbook/marketing/field-marketing/#items-required-by-fmm-to-request-moving-an-issue-from-plan-to-wip) are complete, ping your FMC in a comment to request moving the issue from [Plan to WIP](https://about.gitlab.com/handbook/marketing/field-marketing/#moving-from-plan-to-wip).
* [ ] Once date has been confirmed, FMM will move forward with creating their optional sub-issues listed in the epic code.
* [ ] Once landing page is final and description of workshop has been specified in the FMC Checklist above, FMM to ping FMC to add workshop to the Events Page.
* [ ]  All calls scheduled or covered in regular synch meetings with attendees
   * [ ]  Kick-off call with all DRI's scheduled 6 weeks pre-event
   * [ ]  Prep call scheduled 1 week pre-event
* [ ]  SFDC campaign and Marketo program will be linked in the epic by the FMC once created - if there is an RSVP list, then only include those who have registered. 
* [ ]  Slack channel created and staff/attendees invited
* [ ]  All Instructors identified 
* [ ]  All Instructors calendar hold
* [ ]  MC Secured
* [ ]  Pre-Event Survey Updated for event 
* [ ]  Canned questions confirmed and provided to FMC
* [ ]  Post Event Survey Updated for event 
* [ ]  Identify if offering Incentives for completing survey
* [ ]  Graphics developed for Social media/Sales Invitation
* [ ]  Social Media issue created and submitted
* [ ]  Confirm all presentations completed

## :iphone: Event promotion
* [ ] Social issue created
* [ ] LinkedIn campaign setup

## :busts_in_silhouette: Staffing 
*Make sure you have manager approval to attend and once you commit to an event, please make sure to plan to attend.*

- [ ] Presenter/Lead SA:
- [ ] Presenter 2:
- [ ] Presenter 3 (optional):
- [ ] Q&A Coordinator:
- [ ] Q&A Support 1:
- [ ] Q&A Support 2:
- [ ] MC:
- [ ] Host:

### Requesting Support 
- SA: If you need an SA to attend, [please follow their triage process](https://about.gitlab.com/handbook/marketing/events/#requesting-technical-staffing). 
- PMM: If you need PMM support, [please follow their triage process](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport).
- Speaker: If you need a customer speaker, [please follow the Customer Reference Process](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event). 

   
## :memo: Content 

| Presenter| Bio | Slide Deck/Video Link | Project Link (if any) |
| ------ | ------ | ------ | ------ |
|  |  |  | | |
|  |  |  | | |
|  |  |  | | |

## GitLab virtual backgrounds
* [Option 1](https://drive.google.com/drive/folders/1LC60VGmiBkrCURa7q50aduFgaLB2SAVu?usp=sharing)
* [Option 2](https://drive.google.com/drive/folders/1Fv6_e_1dgSDE5N_KuMvtDM6gdNIUgRcT)

## :hourglass_flowing_sand: Deliverables
     
*Examples below, please fill in chart with details pertaining to your event to help track deliverables.* 

| Item | DRI | Due Date | Status |
| ------ | ------ | ------ | ------ |
| Copy for Landing Page | | | |
| Finalized Agenda |  | | |
| Copy for Emails (1 & 2) | | | |
| Copy for 1-week Reminder Email | | | |
| Polling Questions | | | |
| Content Completed (Demos, Videos, Slides) | | |  |
| MC Script | | | |
| Transition Slide Deck | | | |
| Internal Question Triage Doc/Spreadsheet | | | |
| Pre Event Survey |  | | |
| Post Event Survey | | | |

## :package: Swag/Raffle Items 
List out swag details here and tag the FMC if applicable.

## :moneybag: Financial
* [ ] Will this campaign include the use of Marketing Development Funds (MDFs)? More details on the MDF program can be [found here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds). 
   * [ ] YES/NO
   * [ ] If YES, please tag the Channel Marketing Manager and the Channel Account Manager (CAM) `here`
* [ ]  Spend added to Allocadia activity plan
* [ ]  Coupa requisition created by FMC - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	   

## :checkered_flag: Post Event
* [ ]  Leads cleaned up (24 hours after event close) and shared in the MktgOps Lead List Upload issue
* [ ]  List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign 
* [ ]  After event follow up launched
* [ ]  Event recap & feedback requested from GitLab participants and filled out in the Field Marketing Template linked above in the Planning Spreadsheet & Important Links section. 
* [ ]  Post event close out:
     - [ ] Consolidate Q&A
     - [ ] Consolidate Event Survey
     - [ ] Build final report for Upload
     - [ ] Pick Winners of Prizes

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/labels ~"Events" ~"Field Marketing" ~"mktg-status::plan" ~"Webcast - GitLab Hosted" 
