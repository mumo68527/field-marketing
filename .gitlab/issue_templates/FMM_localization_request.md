### Ask
As a Field Marketer, I request the translation of an asset for countries outside of P0 and P1 using Field Marketing budget. I confirm that I considered using English content and concluded that translated content will increase the ROI.

### ROI details 
Please add a justification as to why this asset needs to be translated including a list of target accounts you will be going after. 

### Details
- Link to campaign this asset will be used for: 
- Type: [PDF, blog, landing page, email, other]
- Target language: 
- Link to asset:
- Translation due date: 
- Estimated costs: 
- Link to line item in the budget file: 
- Campaign Tag: Reminder to ping Finance to create the tag 

### Detailed description of the request

* [add description]

#### Link to documentation, comment or reference
* [insert link]

# Next steps

- [ ] Assign to your functional leader to review - Funcational leader to mark off approval then add WW Director of Field Marketing
- [ ] Reviewed and approved by WW Director of Field Marketing
----