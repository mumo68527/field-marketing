<!-- Purpose of this issue: To request a sales-nominated email to be created via Marketo and set up for regular sends. -->

## Submitter Checklist
* [ ] Name this issue `Sales Nominated Invite: <tactic/campaign name>` without including the brackets (ex. Sales Nominated Invite: Security Workshop)
* [ ] Due date: Set *desired* due date for first send, also in "logistical details" below (dependent on this submitter checklist being completed *at least 5 business days prior*) based on [workback timeline helper](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) - [see handbook for more information about due dates and SLAs](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#turnaround-time-and-slas)
* **Provide logistical details:**
   * [ ] Requested First Send Date: 
   * [ ] Requested Daily Send Time: 
   * [ ] Requested Cadence: `default = M-F`
   * [ ] Reviewers/Approvers (include GitLab handle AND email address ): 
   * [x] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/email-management/#send-frequency)? Sales Nominated Emails MUST be `Non-Operational`
* **Provide email setup details:**
   * [ ] From Name: `default: GitLab Team`
   * [ ] From Email: `default: info@gitlab.com`
   * [ ] Respond-to Email: `default: info@gitlab.com`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable by the emails listed in the copy doc, note: copy must be final ([see handbook](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#-turnaround-time-and-slas))
* [ ] If this section (Submitter Details) is fully complete when issue created, change ~"FMM-Verticurl::blocked" label to ~"FMM-Verticurl::triage"
* [ ] Add your regional label (AMER, EMEA, or APAC) and assign to your direct manager

## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked. Once confirmed, change ~"FMM-Verticurl::triage" to ~"FMM-Verticurl::wip" and assign Verticurl Project Manager.

## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked
* [ ] Create the email in Marketo (or review if already pre-set template)
* [ ] Send test email to the requester and designated additional reviewer/approvers in the "Reviewers/Approvers" section of Submitter Checklist above
* [ ] When test email sent, change label to `~FMM-Verticurl::review`
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Schedule recurring email - [Handbook Instructions](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/emails-nurture/#sales-nominated)
* [ ] Comment to confirm that the email is scheduled to recur
* Close out this issue.

/label ~"FMM-Verticurl::blocked"
