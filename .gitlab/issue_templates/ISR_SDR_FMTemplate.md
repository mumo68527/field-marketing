# Instructions
This issue is required to be filled out by the FMM & ISR/SDR leadership for all owned events (either virtual or in-person) at least **45-days prior** to the event. If it is not an owned event, then this section is optional. 

**Reminder:** Add this issue to the epic related to your FMM issue.

`Name this issue using the following format, then delete this line: [ISR/SDR Request] - [Event or Tactic Name] - [3-letter Month] [Date], [Year]`

## [Main FM Issue >>]()

## :busts_in_silhouette: DRIs

- [ ] FMM:  
- [ ] FMC:  
- [ ] **ISR Manager:** FMM to assign    
   * [ ] ISR Manager to assign ISR DRI here if they are responsible for outreach and also assign to this issue    
- [ ] **SDR Manager:**  FMM to assign      
   * [ ] SDR Manager to assign SDR DRI here if they are responsible for outreach and also assign to this issue        

## 1. Field Marketing Brief
- [ ] Requestor to define asks of SDRs / ISRs 
- [ ] FMC to add URL from `Follow-up email` or `copy doc` from the epic, so everyone is aligned on the follow-up message and the offer that is being made

#### Pre-Event Request(s) Defined 



#### Post-Event Request(s) Defined 



## :trophy: Goals for ISR/SDR Team 
*ISR/SDR project lead, please review the full issue to get an understanding of the event and fill out the goals below. Please review the [handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sts=Event%20Promotion%20and%20Follow%20Up%20Assistance) for more information on this process.*

## 2. SDR Pre-Work

 * [ ] Report Pre-Event Invitation List (Leads): [AMER Template](https://gitlab.my.salesforce.com/00O4M000004acF6), [EMEA Template](https://gitlab.my.salesforce.com/00O4M000004aPk3), [APAC Template](https://gitlab.my.salesforce.com/00O4M000004acHv)
 * [ ] Report Pre-Event Invitation List (Accounts & Contacts): [AMER Template](https://gitlab.my.salesforce.com/00O4M000004adZv), [EMEA Template](https://gitlab.my.salesforce.com/00O4M000004aRFx), [APAC Template](https://gitlab.my.salesforce.com/00O4M000004adaA)
 * [ ] Pre-Event Invitation Sequence: [Template](https://accounts.outreach.io/users/sign_in)
 * [ ] Report Non-MQL Registrants (Leads): [Template](https://gitlab.my.salesforce.com/00O4M000004aPGQ)
 * [ ] Report MGL Registrants (Leads): [Template](https://gitlab.my.salesforce.com/00O4M000004aRGC)
 * [ ] [Outreach Sequence Library - creator: Mike Lebeau](https://docs.google.com/spreadsheets/d/1pF_nZfcnzbNWIpe6Gf1WW9pe66hhnbuL1X3CraUCuVw/edit?ts=5fbec3d1#gid=0) 
 * [ ] [FY22 Events/Campaigns Collection in Outreach](https://app1a.outreach.io/sequences?smart_view=8546) 

## Final Outreach Sequences 
SDR DRI(s) to fill out the following information once the sequence has been approved and is live. If not using the checkboxes below, then please delete row(s). 

- [ ] Pre-Event Outreach Sequence: `Insert URL here`
- [ ] Post-Event Outreach Sequence - Attended: `Insert URL here`
- [ ] Post-Event Outreach Sequence - No Showed: `Insert URL here` 

When you complete the sequences (with approvals from manager and FMMs), please share the sequence in #sdr_global and other relevant SDR Slack channels

- [ ] DRI to check off as complete once the slack announcement has been made 

## Assignments and Labels

* `FMM to assign themselves`
* `FMM Assign to the appropriate ISR or SDR manager`
* `FMM Assign the regional SDR Awareness label to the issue - SDR::AMER Event Awareness, SDR::APAC Event Awareness, SDR::EMEA Event Awareness`
* `FMM Assign the SDR-GO Live label once the issue is fully completed` 
* `If this is a PubSec request, FMM assign the SDR Pub Sec label to the issue`
* `If this is a West SDR request, FMM assign the SDR West Staff Request to the issue`

/label ~"Field Marketing" ~"mktg-status::plan" 
