## :notepad_spiral: Event Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **FMM/Event Owner:** 
* **FMC:**
* **Onsite DRI:**  
* **Tactic/Event Type:** [**Conference**](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#conference)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Official Event Name:** 
* **Date:**
* **Location:** 
* **Start/Stop Time and Time Zone for Event:**  
* **Event Website:**
* **Landing Page:**
* **If we sponsored last year, link to SFDC campaign:** 
* **Allocadia ID (line item ID used for tactic charges):** (to be filled in by the FMC)
   * [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
* **Campaign Name (Marketo/SFDC):** (to be filled in by the FMC)
* **SAO Goal:** 
* **Budgeted Costs (sponsorship + auxiliary cost):** 
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/ƒUKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## Splitting Tactics
*This section is to be filled in if you are splitting your tactic between sub-regions. Please make sure each activity plan's line item panels match and are completed before submitting a contract request to your FMC.*
* **Lead FMM:** (please specify the FMM owner for SFDC here)
* **Allocadia IDs:** (please list the Allocadia category or sub-category IDs in each activity plan here for your FMC to reference)
* **Budget Breakdown:** (please list all sub-regions participating and what cost is allocated for each sub-region)

## :sleuth_or_spy: Description of Event
(Description) 

## :vertical_traffic_light: User Journey
(FMM to provide a description of the user journey)

## :paperclip: Planning & Recap Spreadsheet 
Make a copy of the [Field Marketing Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1) and save here. When doing so, just delete out this entire line and leave the link back to the document itself. NOTE: Be sure the document can be edited by anyone at GitLab. 

## :white_check_mark: FMC Checklist 
* Will leads be provided for upload? `YES/NO`
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists
* Please specify if follow up is requested, including send date and time/time zone:
  * [ ] Follow up: `Date/Time/Time Zone`
    * Do you want to send different follow up emails to different audiences? `YES/NO` (If YES, you must provide copy for the different emails in your copy doc and have specified audiences for each email send)
* Add to event calendar? YES
* Add to the [GitLab Events Page](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)? `YES/NO`
   * If YES please link or add appropriate event description/summary of event to be added to the events page here: `Event description`  

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the FMM is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate to the SDR issue to the epic. 

## :construction_site: Prepare
* [ ] Once [these items](https://about.gitlab.com/handbook/marketing/field-marketing/#items-required-by-fmm-to-request-moving-an-issue-from-plan-to-wip) are complete, ping your FMC in a comment to request moving the issue from [Plan to WIP](https://about.gitlab.com/handbook/marketing/field-marketing/#moving-from-plan-to-wip).
* [ ]  If an [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) is needed, FMM to fill out and submit.
* [ ]  All calls scheduled or covered in regular synch meetings with attendees
   * [ ]  Kick-off call with all DRI's scheduled 6 weeks pre-event
   * [ ]  Prep call scheduled 1 week pre-event
* [ ]  Logo, company description and artwork sent to organizer [Company description, Product messaging and 
GitLab positioning](https://about.gitlab.com/handbook/marketing/product-marketing/), [GitLab Logos](https://about.gitlab.com/press/#press-kit)
* [ ]  Press list requested and shared with GitLab PR (Highwire PR for NAM, speakeasy for EMEA)
* [ ]  Slack channel created and attendees invited
* [ ]  Save the Date sent for staff
* [ ]  Staff registered to event
* [ ]  All travel and hotel accomodations have been booked and added to the Planning Spreadsheet

## :calendar: Event Schedule
(add schedule/agenda here)

## :pushpin: General Event Details 
* **Arrive no later than:**    
* **Table/Setup** (please set up the table when you arrive):
* **Audio Visual/Demo Setup:** NOTE: we normally buy a TV & USBC to HDMI cable to use as a monitor for the show & then raffle off the TV
* **Pitch Deck:**      
* **QR Code** (FMM to provide [QR code](https://about.gitlab.com/handbook/marketing/events/#amer-field-marketing-qr-codes-for-events) to onsite POC to print out for tablestop stand):
* **Passport Program Details:**           
* **Tracking:** Take notes on the people you talked to and what you learned about them. Take them on your phone, if possible, for easy upload post event. You are expected to fill out the event recap. 

## :trophy: Raffle Details
* In an effort to gather people’s contact info, we conduct a raffle for the TV on `date` at `time` (during the last break). 
* General information regarding logins for the iPad can be found [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#amer-field-marketing-ipad-purchasing-and-setup-instructions) if needed.
* iPad provided to onsite DRI (PW is 111820). FMM to create a Google form for the raffle sign up (`LINK FORM HERE`) and also add to the homepage on the iPad. 
* At `TIME` and `DATE`, onsite DRI to open the responses spreadsheet from the Google form (`FMM TO LINK RESPONSE SPREADSHEET HERE`).
* Onsite DRI to go to the [random number generator](https://www.google.com/search?q=random+number+generator&oq=random+nu&aqs=chrome.0.69i59j69i57j0l4.1982j0j7&sourceid=chrome&ie=UTF-8) and enter the number of the first cell and last cell in the spreadsheet. Remember to start with 2 as row 1 is a header row in the spreadsheet and you don't want that row to be selected. 
* From there, the random number generator will pop out a number. Reference that number row in the spreadsheet to get the name of the winner. They must be present to win. Keep repeating the process until you have a winner.
  
## :tropical_drink: Happy Hour Details
* **When:**  
* **Where:**  
* **Sponsors:**  
* **Promo Plan:**  
* **Lead Collection:**   

## :moneybag: Financial
* [ ] Will this campaign include the use of Marketing Development Funds (MDFs)? More details on the MDF program can be [found here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds). 
   * [ ] YES/NO
   * [ ] If YES, please tag the Channel Marketing Manager and the Channel Account Manager (CAM) `here`
* [ ]  Spend added to Allocadia activity plan
* [ ]  Coupa requisition created by FMC - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	 

## :hourglass_flowing_sand: Deliverables
*Examples below, please fill in chart with details pertaining to your event to help track deliverables.*

| Items                                     | Due Date     | Completed | 
|-------------------------------------------|--------------|-----------|
| Swag ordered                              |              |           |
| Event assets ordered                      |              |           |
| Staff List                                |              |           |		

## :busts_in_silhouette: Staffing 
Once you commit to an event, please make sure to plan to attend.  
   * [ ] SAL:
   * [ ] SDR:
   * [ ] SA: If you need an SA to attend, [please follow their triage process](https://about.gitlab.com/handbook/marketing/events/#requesting-technical-staffing). 
   * [ ] PMM: If you need PMM support, [please follow their triage process](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport).
   * [ ] Speaker: If you need a customer speaker, [please follow the Customer Reference Process](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event). 
   * [ ] other:  

## :airplane: Travel
Make sure you have manager approval and [COVID committee approval](https://about.gitlab.com/handbook/travel/#policy-and-guidelines) to attend
   * [ ] **Travel Dates:**
   * [ ] **Link to hotel block or recommendation (if applicable):**
   * [ ] **DEADLINE to book your travel:**
   * [ ] **Travel details added to event sheet**

## :package: Swag and Event Assets
*Please note that we have DevOpsDays kits in the Nadel warehouse to be utilized specifically for these events. Please ship one kit per event, along with your swag selections (details in the Nadel portal under Event Assets section).*

### Shipping to Event  
* **Shipping Address:**   
* **Point of Contact:**  
* **Deadline for Shipments:**  
* **Items Ordered/Quantities/Vendor** (example: 100 notebooks from Nadel):  
* **Tracking Number:**    
* [ ] **Items Received**  

### Return Shipping
* **Onsite DRI Responsible for Return Shipping:**  
* **Nadel return shipping label provided?** YES/NO  
*   **If no:**  
	* **Address for Return:**  
	* **Fedex Account Number:** Available in marketing 1pass if needed
* **Return Tracking Number** (Provided by Onsite Event DRI):  
* [ ] **Items Shipped Back**  

## :iphone: Event Promotion
* [ ] Social issue created 
* [ ] LinkedIn campaign setup

## :checkered_flag: Post Event
* [ ]  Lead list received from organizer
* [ ]  All pictures uploaded to Google Drive
* [ ]  List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign 
* [ ]  Lead list cleaned and uploaded and add to nurture submitted
* [ ]  Follow up email triaged
* [ ]  Event recap provided by staff in the Planning Spreadsheet

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Events" ~"Field Marketing" ~"mktg-status::plan" ~"DevOpsDays"
