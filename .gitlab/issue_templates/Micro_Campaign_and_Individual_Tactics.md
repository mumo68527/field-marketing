## :notepad_spiral: Details  

Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.  
**Only tag individuals below if there is an action item for the team.**
* **FMM:** @fmmusername
* **FMC:** @fmcusername
* **Sales Territory**: @username(s)
* **SAL(s) or RD**: @username(s)
* **Date Range:**  
* **Tactics Included:** Please list all tactics you will be including utilizing [**THIS CAMPAIGN TYPE LIST**](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#campaign-type--progression-status) and/or [**THIS DMP CAMPAIGN TYPE LIST**](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#program-definitions). You must select tactic names from the lists provided. If you are not sure how to categorize your tactic, please reach out to your FMC or the Campaign Management team to discuss.
  *  **Other Tactic** - If you have an idea for a new tactic that involves creating a new campaign type, please create an issue in the [MOps project](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues). Please be as detailed as possible and be aware that creating a new campaign type will take planning and time.
* **Budgeted Costs (Complete Cost):** This is the sum of all tactics included in the micro-campaign. Individual tactic costs will be listed in your tactic issues/epics.
* **SAO Goal:**
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## :paperclip: Planning & Recap Spreadsheet

Make a copy of the [Field Marketing Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1) and save here. When doing so, just delete out this entire line and leave the link back to the document itself. NOTE: Be sure the document can be edited by anyone at GitLab. 

## 🛣 User Journey
Explain how these tactics work together to drive engagement with the target accounts. What is the end-user's journey from start to end?

## :baseball: Intent Data and DemandBase

**ABM Requests**  
To request a campaign please open the [`Demandbase_Campaign_Request_Template Issue`](https://gitlab.com/gitlab-com/marketing/account-based-strategy/demandbase/-/issues/new?issuable_template=Demandbase_Campaign_Request) and link to this issue. 

**SLA**   
Please note we need a minimum of a two week lead time to execute from when all information is complete in this issue + any additional time needed for creative to be done, etc.   

## :iphone: Digital Marketing Team 

**Digital Marketing Team Requests**   
To submit your request, please open an issue utilizing the [Paid Ads Issue Created for Digital Marketing](https://gitlab.com/gitlab-com/marketing/demand-generation/digital-marketing/-/issues/new?issuable_template=paid-digital-request) - FMM creates, assigns to FMM and DMP Team  and link to this issue.

**SLA**  
Ideally, we need at least 3-4 weeks before the proposed launch date in order to plan strategy, forecast goals, and secure all creative assets.  

## :two_women_holding_hands: Channel Partner Section 
This section must be filled out if you are running this campaign with a channel partner. 

- [ ] Please add the name of the channel partner to the title of this issue. 
- [ ] Is the partner speaking? YES/NO    
- [ ] Is the partner driving registration? YES/NO   
- [ ] If the partner is driving registration, you must provide them with a unique UTM. 
   - [ ] Please add the unique UTM here: example: https://page.gitlab.com/pies-in-july-virtual-pizza-registration-page.html?utm_medium=email&utm_source=wwt&utm_campaign=piesinjulyfy22q2&utm_content=partnerlandingpage
   - [ ] Please add the SFDC UTM tracker: [example](https://gitlab.my.salesforce.com/00O4M000004oMS7). 
- [ ] Is the partner standing up the landing page for registration?  YES/NO   
- [ ] Is the partner supplying graphics? YES/NO  
   - [ ] If yes, have these graphics been approved by the design team? YES/NO    
- [ ] In Person Event? YES/NO
   - [ ] Are we in a Partner's Booth or Pavillion: YES/NO
      - [ ] If yes, which one? BOOTH/PAVILLION
   - [ ] Do we set up our own booth: YES/NO
   - [ ] Is our participation at a kiosk: YES/NO

### Lead follow-up with Channel Partner 
- [ ] Will the partner do initial lead follow-up? YES/NO   
   - [ ] If yes, will GitLab receive leads at any point?   
   - [ ] Please describe how the partner plans to follow-up on leads received from this event/campaign:    
- [ ] Please add any additional, relevant notes regarding the partnership on this event/campaign here: 

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`
* [`Additional Labels`](https://gitlab.com/groups/gitlab-com/-/labels)

/label ~"FMM-MicroCampaign" ~"FMM-Other Tactics" ~"Field Marketing" ~"mktg-status::plan" 
 
