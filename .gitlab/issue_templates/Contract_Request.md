## General
- **FMM Owner:**      
- **Link to Field Marketing Issue (required before processing Coupa req):** 
- **List or link to Allocadia ID(s) (please ensure all information has been included in your line item and in the details panel):** 
- **Is your cost per MQL more than $500?** YES/NO
   - If **YES**, then please document the business reason why this investment needs to be made: 
- **Did we sponsor this event last year?** YES/NO
  - If **YES** please confirm name and date of event for Sisense reporting here:
- **Is this tactic going to be split between regional teams?** YES/NO
  - If **YES** please specify which teams and how the cost will be split. Please also confirm each team member has included the tactic in their Allocadia activity plan.

## Contract Details
*Please make sure you have reviewed your contract carefully (terms, sponsorship level, cost, date, etc.) and either pre-fill the contract details required in the agreement before sending to your FMC or specify all field details in the section below so your FMC has the correct information for processing.*

- **Vendor Name:**
- **What is the sponsorship level/cost?** 
  - **If a booth is involved, what is the measurement/booth size and total cost of the booth?**
- **Vendor POC Name/Email (the person you have been working with):** 
- **Vendor Billing POC Name/Email (if not available, FMC will contact vendor to confirm):** 
- **Attach contract here:** 
- **Attach sponsorship prospectus here (if not included in contract):** 
   - *FMC to attach both contract and prospectus in supporting documents in Coupa req*

## Shared Information

**Does vendor have access to [red or orange data](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#data-classification-levels)?** 
* [ ] **No non-public data will be stored or processed**
    * *Non-public data means anything not currently available in our handbook or anything you would be reluctant to add to our handbook.*
* [ ] **Yes, contract does involve storing or processing of non-public data**
    * *This includes GitLab sending names/emails to vendor from SFDC for invites or swag/event items.*
    * *If **YES**, FMM or FMC to send vendor the [Physical Services Guidelines HB Link](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-minimum-security-standards.html#third-parties-providing-physical-services-field-marketing) and have them agree to terms in an email. The FMC will then include a screenshot of the email in the procurement issue or Coupa submission.*
        * *Note: If a vendor has gone through a security review within the past 12 months, they will not need to be reviewed or sent the guidelines again.*

## Software Vendors
- **Is this a software or new technology contract?** YES/NO
  - If **YES** open a [**MOps Tools Eval Issue**](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=tools_eval) and attach to the Field Marketing issue. The MOps Tools Eval issue is required *before* submitting the procurement issue.
  - If **YES** is integration required with existing applications (SFDC, Marketo, Outreach, etc.)? YES/NO
- **Description of Product (please provide a clear description to help determine correct finance template to be utilized):**

## Assign
`Assign to your regional FMC`  
/label ~"Field Marketing" ~"mktg-status::plan"
