## :notepad_spiral: Event Details 
* **FMM/Event Owner:** 
* **FMC:**
* **Onsite POC for non C-Suite Meetings:**  
* **Onsite POC for C-Suite Meetings:**  
* **Name of Conference:** (please mark this issue as related to the main conference issue)
* **Date:**
* **Meeting Space Location:**

## :calendar: Due Date for Scheduling
All meetings need to be scheduled and approved by (`add date/time`).  After that time, the doc will be locked and any changes and/or additions will need to be made onsite by `add onsite FM POC` for non C-suite meetings and `add onsite C-suite POC` for C-suite meetings.

## :file_folder: Briefing Doc
- [Briefing Doc Template](https://docs.google.com/document/d/1rfuK-A3fNPLW92ENdd7lt-qc2Cb-ol_p39XZNFb0pFc/edit#)
- AE to make a copy of the above for each meeting and follow the below instructions

## :paperclip: Meeting Setting Doc
- [ ]  FMM to make a copy of this [Meeting Setting Template](https://docs.google.com/spreadsheets/d/19QCNUFr5NMDJs_OYB4XskvxlXmFZFogCcaRY0XjkIlk/edit#gid=204316214) and include a link to the spreadsheet in the main conference plannning doc for easy access.
- [ ]  Change settings of doc to `comment only`
- [ ]  **Meeting Setting Doc** - `link doc here`

## :notebook: General Directions
The meeting setting doc is comment only, so when entering your meeting info, you will need to tag the FMM (for non c-suite) for the meeting to be approved.  This avoids confusion and overrides of existing meetings. For C-suite meetings, your first reach out should be directly to the EA of the person you are trying to schedule with and they will coordinate the booking of the meeting.

### Directions for Booking (if non C-suite):

1. Once you have a meeting time, enter your meeting with full customer information into the meeting setting doc linked above in this issue (including GitLab attendees) in a comment in the time field you are requesting. If the meeting is pending, please put pending in parenthesis beforehand so we know it is a placeholder (once confirmed, remove this).
1. AE completes and attaches a completed copy of the [Briefing Doc](https://docs.google.com/document/d/1rfuK-A3fNPLW92ENdd7lt-qc2Cb-ol_p39XZNFb0pFc/edit#) to the timeslot.
1. Assign your timeslot in the doc for approval to the FMM in charge. The FMM will approve ASAP, but no later than 24hours after the request is made.
1. Once the meeting is approved, send calendar invite to all included (customer, GitLab attendees and FMM) in the meeting and include the address.  Directions to add for attendees are below and should be copy and pasted into the invite.

### Directions for booking (if includes C-suite)

1. Once you have a meeting time, enter your meeting with full customer information into the meeting setting doc linked above in this issue (including GitLab attendees) in a comment in the time field you are requesting. If the meeting is pending, please put pending in parenthesis beforehand so we know it is a placeholder (once confirmed, remove this).
1. AE completes and attaches a completed copy of the [Briefing Doc](https://docs.google.com/document/d/1rfuK-A3fNPLW92ENdd7lt-qc2Cb-ol_p39XZNFb0pFc/edit#) to the timeslot. Note: External contact should be a director level and above to meet with members of our C-Suite (CEO, CRO, CMO, CFO, CCO). 
1. If EA determines we are proceeding with the meeting, sales rep introduces EA via e-mail to their external contacts and EA blocks the time in the planning spreadsheet as a *HOLD*. 
1. EA will coordinate the meeting with external party and send invite from Exec's calendar, the [Briefing Doc](https://docs.google.com/document/d/1rfuK-A3fNPLW92ENdd7lt-qc2Cb-ol_p39XZNFb0pFc/edit#) will be shared in this invite with all parties. Once the meeting is confirmed and scheduled the EA will update the planning spreadsheet with `CONF.` EA and sales rep to be on-site to staff the meeting at larger conferences (AWS/DOES/KubeCon). If sales rep will not be attending the meeting in-person, they need to notify the EA ASAP. 

### :memo: Post Meeting
AE to please update the meeting notes doc no later than 24 hours post-meeting.

## :mailbox: Directions for Meeting Attendees 
(Specify instructions for attendees here to be included in confirmation email)

## TO DO

* `Assign the issue to the FMM and EA`
* `Assign the Quarter and Region tags to the issue`


/label ~Events ~"Field Marketing" ~"mktg-status::plan" ~"Meeting Setting"


