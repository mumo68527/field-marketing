<!-- Purpose of this issue: For FMCs to create Marketo program and SFDC campaign. -->

## For Webcasts/Workshops Only
* [ ] Set up Zoom (this should be completed *before* setting up program tracking below)

## Program Tracking Action Item Checklist
* [ ] Name this issue `Program Tracking: [name of campaign]` (ex. Program Tracking: Modernize CI/CD Webcast)
* [ ] Set a due date for the issue based on the [SLA Workback Schedule](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)
* [ ] Create Marketo program - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#marketo-program-and-salesforce-campaign-set-up)
* [ ] Create Salesforce campaign - [process to follow](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#marketo-program-and-salesforce-campaign-set-up)
* [ ] Add Marketo and SFDC links to epic
* [ ] Close out this issue.

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"Field Marketing"
-->
