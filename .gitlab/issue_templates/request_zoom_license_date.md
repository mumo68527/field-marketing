<!-- Purpose of this issue: To request reservation of a date for the Zoom license for Field Marketing-run GitLab-hosted virtual events (webcasts and workshops). -->

## Submitter Checklist
**{+NOTE: Please make sure your webcast or workshop date is confirmed and final when submitting this issue. Moving a webcast or workshop date is extremely time-intensive once setup is complete.+}**

- [ ] Name this issue `Zoom License Date Request: <name of virtual event>` (ex. Zoom License Date Request: Mastering CI/CD Webcast)
- [ ] Due date: Set due date based on the [SLA Workshop Schedule](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029)
- [ ] Owner Username: @
- [ ] Virtual Event Type (select one):
    - [ ] Buyer Progression Webcast (`if selected, please also fill in the Webcast versus Self-Service Virtual Event section below`)
    - [ ] Virtual Workshop  
      - [ ] New copy  
      - [ ] Existing copy
- [ ] If there is a hands-on lab component, assign `@jeffersonmartin` for awareness 
- [ ] Estimated [number of users](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#gitlab-hosted-virtual-events-details) expected: `List max number here`
- [ ] Field Marketing Issue `Link issue here`
 - [ ] Requested Event Date: `YYYY-MM-DD`
- [ ] Requested Time: `09:00am-12:00pm`   Time Zone: `ET/PT/CET/UTC,etc`
- [ ] Topic: `Add tentative name/topic`
- [ ] Virtual Workshops - Lead SA and Lead FMM must be identified and confirmed to schedule the workshop.
  - [ ] Lead SA:
  - [ ] Lead FMM: 
- [ ] Buyer Progression Webcasts - All speakers must be identified and confirmed to schedule the webcast. For non-GitLab speakers, please list their first/last name and email address below.
  - [ ] Speaker 1
  - [ ] Speaker 2   

## Webcast Versus Self-Service Virtual Event 

If you selected Buyer Progression Webcast above, please visit the [Virtual Events Decision Tree](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#gitlab-virtual-events-decision-tree) and additional details listed out in the [Virtual Events](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#virtual-event-types) page to review the differences between a GitLab-hosted Webcast (Buyer Progression Webcast) and a Self-Service Virtual Event (with or without promotion) and answer the below questions. 
- [ ] Do you want attendees to be able to unmute themselves and/or share their camera? YES/NO
- [ ] Are you expecting more than 200 participants? YES/NO
  - If NO, please provide justification for why you would like to run a Buyer Progression Webcast versus a Self-Service Virtual Event:

## Assignments
* [ ] Submit to your regional FMC/FMS

## FMC/FMS Checklist  
* [ ] Confirm that date is available (or not) to requester
* [ ] Add to calendar when final date is agreed (include FM issue link) - Remember to book 30 mins before and 30 mins after the slotted time. 
* [ ] Add Lead SA/Speakers/FMM to calendar invite.
* [ ] Comment to requester that date is secured and on the calendar
* [ ] Close out this issue


<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"Field Marketing"
-->
