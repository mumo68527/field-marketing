<!-- Purpose of this issue: To complete pre-prepared canned or polling questions, link webcast presentation deck, obtain the redemption code and finalize pre and post event surveys for a webcast/workshop. -->

## :pencil: Prep Tasks (DRI: FMM @)  
* [ ] **Determine three "canned" Q&A questions (required only for Webcasts)**   
*These questions are not added to Zoom, they should be kept on-hand and ready to ask during the webcast.*    
      - Question 1:  
      - Question 2:  
      - Question 3:  
* [ ] **Determine polling questions (required for both Webcasts and Workshops)**     
*Helpful Hint: Include additional polling questions asking how attendees are doing that can be utilized throughout the webcast or workshop as a check-in.*  
      - Poll 1 Question:  
        Answer 1:  
        Answer 2:  
        Answer 3:  
        Answer 4:  
      - Poll 2 Question:   
        Answer 1:  
        Answer 2:  
        Answer 3:  
        Answer 4:  
      - Poll 3 Question:  
        Answer 1:  
        Answer 2:  
        Answer 3:  
        Answer 4:  
* [ ] **Ping the FMC in this issue once your polling questions are complete.**

## Prep Tasks for Workshops Only (DRI: Presenter 1/Lead SA @)
* [ ] Presenter 1/Lead SA to provide redemption code here: `redemption code`
* [ ] Ping the FMC in this issue once redemption code has been provided.

## FMC Tasks (DRI: FMC @)
* [ ] FMC to clone appropriate workshop Pathfactory track and add URL to Marketo token (for workshops only).
* [ ] FMC to add redemption code to Marketo token.
* [ ] FMC to add redemption code to Dry Run Agenda.
* [ ] FMC to add polling questions to Zoom.
* [ ] FMC to close out issue once all tasks are completed.

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"Field Marketing"
-->
