<!-- Purpose of this issue: To request a landing page be created in Marketo with relevant marketing automation. -->

## Submitter Checklist
* [ ] Name this issue `Marketo LP and Automation: <tactic/campaign name>` (ex. Marketo LP and Automation: Security Workshop)
* [ ] Due date: Set due date based on Verticurl SLA ([at least 5 business days prior](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/agency-verticurl/#triage-steps-fmm)) 
* [ ] Review [landing page best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages/#landing-page-conversion-best-practices)
* **Type of Marketo landing page** (select one)
   - [ ] Webcast
   - [ ] In-person on-site event registration
   - [ ] Virtual Workshop
   - [ ] Direct Mail
   - [ ] Survey
   - [ ] Executive Roundtable
   - [ ] Self-Service
   - [ ] Sweepstakes
* **Please specify if your LP requires the below** (copy must be provided in the copy doc)
  - [ ] Thank you/confirmation page
  - [ ] Confirmation email
  - [ ] Add to calendar feature in confirmation email
* [ ] Provide Final Copy: `add link to final copy googledoc` (make sure the document is editable by the emails listed in the copy doc)
* [ ] Reviewers/Approvers (email addresses must be provided): 
* [ ] Provide speaker image (200x200) for Webcasts only:
* [ ] If this section (Submitter Details) is fully complete when issue created, change ~"FMM-Verticurl::blocked" label to ~"FMM-Verticurl::triage"
* [ ] Add your regional label (AMER, EMEA, or APAC) and assign to your direct manager

## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked. Once confirmed, change ~"FMM-Verticurl::triage" to ~"FMM-Verticurl::wip" and assign Verticurl Project Manager.

## FMC To Do (once LP is reviewed/approved)
* [ ] Add LP URL to top of the epic
* [ ] Add event to the [events page](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents) if requested by FMM in FMC checklist
* [ ] Once event is complete, follow [these instructions](https://about.gitlab.com/handbook/marketing/field-marketing/#process-to-close-marketo-landing-pages-and-landing-page-forms) to request Verticurl close the LP

## Marketo work to be completed 
* [ ] Update Marketo tokens
* [ ] Update registration landing page URL
* [ ] Update thank you landing page URL (if applicable)
* [ ] Update the "resulting page" of the form (to make it the thank you page, if applicable)
* [ ] Activate smart campaign(s)
* [ ] Test live registration page and flows
* [ ] Add registration URL to epic
* Close out this issue.


**Need help getting started? [Learn more in the handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/landing-pages/#marketo-landing-pages-general)**


/label ~"FMM-Verticurl::blocked"
