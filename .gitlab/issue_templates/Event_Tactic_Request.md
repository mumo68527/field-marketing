**This issue template is to be used by anyone internally requesting GitLab sponsor/host an event (in-person or virtual) or tactic (direct mail, survey, paid ads, etc.).**

## Directions

1.  Complete the details below.
2.  Assign to the FMM responsible for the region where the event will take place. FMM regional listing is available [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region).

## General Details

* **FMM Owner:** @fmmusername
* **Name of Event or Tactic:** 
* **Type of Event or Tactic (sponsored webcast, survey, etc.):** 
* **Date:** 
* **Event Website (if applicable):**
* **Level of Sponsorship Requested (if applicable):**
* **Is this an account-centric event or tactic?**
  * **If YES, for which accounts?**
* **Salesforce campaign if we have sponsored in the past:**
  
## Justification for wanting to sponsor or host the event/tactic (please be as detailed as possible)

**What is the goal(s) for the sponsorship?**
* [ ]  Brand awareness
* [ ]  Thought leadership
* [ ]  Partnerships/ Alliances (If YES, please tag the Alliances team)
* [ ]  C-level engagement
* [ ]  Gather new relevant leads/ drive ROI
* [ ]  Educate possible buyers or users on our product or features

## Public Sector Only - REQUIRED  

**Is there a partner you would like to work with on this event/tactic?**
- [ ] Carahsoft
- [ ] DLT
- [ ] Other (please specify)
- [ ] None

**Please specify if you would like this event/tactic to align to a [Current Integrated Campaign](https://about.gitlab.com/handbook/marketing/campaigns/#active-integrated-campaigns).**
- [ ] List specific integrated campaign here
- [ ] Speed to Mission (Efficiency)
- [ ] None


/label ~"Field Marketing" ~"mktg-status::plan"
