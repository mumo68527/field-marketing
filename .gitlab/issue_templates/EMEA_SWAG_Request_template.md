
## :package: SWAG and Logistics

This template is used to request SWAG for giveaway campaigns and field events for EMEA (not for customers or sales).

## Giveway Campaign
* **FMM/Requester:**
* **Type:**
* **Campaign tag:**
* **Budget:**
* **Items requested:** 

## Field Event 
* **Shipping Address:**   
* **Point of Contact (name):**
* **Phone number (this is required):**
* **Deadline for Shipments:**  
* **Items requested:** 
* **Tracking Number (provided by FMC):**  to be added by FMC
* [ ] **Items Received** 


### Return Shipping process  
Please only ship back items like iPad, Banners, backwall, counter top. Keep the SWAG and use it for customers or at other events. 

* **Onsite DRI Responsible for Return Shipping:**  
* **Return shipping label provided?** YES/NO  
*   **If NO:**  
	* **Address for Return:**  TenandOne Event Agentur, Meyerbeerstraße 12, 81247 München, Germany
    * **Phone:** +49 89 2554190
* [ ] **Items Shipped Back** 

/assign @Helenadixon

/label ~Events ~"Field Marketing" ~"mktg-status::plan" ~"FMC EMEA" ~"EMEA"
