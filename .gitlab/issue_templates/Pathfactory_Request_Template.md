<!-- Purpose of this issue: To request a track be created in Pathfactory or a piece of content uploaded to Pathfactory. -->

## General
* [ ] Name this issue `Pathfactory Request: <Tactic Name>` (ex. Pathfactory Request: GitLab Gives)
* [ ] Set due date: 5 business day SLA

## Pathfactory Track Request

* **Submit Content Details:**
  * [ ] [Track Type](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#pathfactory-campaign-tools) (choose one): `Target Track` OR `Recommended Track`
  * [ ] Track Name:
  * [ ] Link issue to related epic
  * [ ] Language of track:
  * [ ] Custom URL Slug: (short name for the track, without filler words like `and`, `the`, etc. - ex. `ci-awareness`)
  * [ ] [Form Strategy](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#form-strategy): YES/NO
    * Please Note: Form strategy is not generally recommended or needed for most Pathfactory tracks. Known users (example - registrants from a self-service event that did not show) that view a Pathfactory track will still receive Bizible touchpoints. Only utilize form strategy if you are trying to reach unknown users such as direct links from digital ads or web promoters).
  * [ ] Does a [UTM](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) need to be added to the Pathfactory track? YES/NO
    * If YES, please provide the following specified in the above Gsheet:
      * Campaign Tag:
      * Campaign Content: 
  * [ ] Content for track (if a target track, please also list in the order you would like content to appear in the track):
    - asset name as appears in Pathfactory
    - asset name as appears in Pathfactory
    - asset name as appears in Pathfactory
    - asset name as appears in Pathfactory

## Pathfactory Upload Request
* **Submit Content Details:**
  * [ ] Content Upload Type (choose one): `URL` OR `File upload (up to 100MB)` OR `URL`
  * [ ] Specify [GTM motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions)
  * [ ] URL Slug: `be descriptive of the content with no stop words such as and/the/etc. (ex. gartner-mq-aro`)
  * [ ] Official Content Title: 
  * [ ] Content Description: `Add clear and concise description (1-2 sentences)`
  * [ ] Content Type: [selection ONE from options here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/content-library/#content-types)
  * [ ] Content Topics: [include all relevant from options here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/content-library/#content-topics)
  * [ ] Content Buyer stage: `Awareness` OR `Consideration` OR `Decision/Purchase`

## Requester
* [ ] Once the above details are complete, assign to your regional FMC


## FMC Checklist
* [ ] [Create Track](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#create-a-content-track) or [Upload Content](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/workshop-webcast-how-to/#uploading-the-presentation-deck)
* [ ] If creating a new track, add to the [changelog](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#changelog)
* [ ] Confirm track or content URL here:
* [ ] Ping requester that request is complete and close out this issue

## Labels
/label ~"Field Marketing" ~"mktg-status::plan"
