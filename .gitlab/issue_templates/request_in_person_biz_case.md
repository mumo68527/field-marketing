Purpose of this issue template is for Field Marketing to share business case for in person events as we reenter from the COVID pandemic and have travel limitations in place. 

#### Please do not link out to separate gdocs. All information should be added directly in the issue. Given this issue will note travel details, it has been marked confidential. 

# Overview 
### Date 
### Location
### GitLab Attendees

List out specific people or number of people & their role - ex. 2 FMMs, 4 SAs, 2 SALs - all within driving distance of the venue.
Attendeed must receive approval to attend in order to submit expense reports. 

### How will attendees get to the event?
 I.e. 1 will drive 2 will fly.  

### Number of overall event attendees

### COVID specific guidelines
List or Link to the specific details

# Timeline to Contract
In this section, please note out any contract we already have in place or if we expect to receive a contract with a tight timeline to make a decision to attend or not attend. 

# Cost to participate 

# Expected ROI 
If we attended this event in the years past, please share the linear contribution to pipeline & link to the details. 
* Customers/prospects targeted to engage with: 

For the below section feel free to take a screen shot of the Allocadia impact modeller, or add the details, whichever you prefer. 

* Target MQLs: 
* Target SAO: 
* Estiamted pipeline: 

# On-Site Marketing Attendance  
If you are a Marketer who is planning on attending this event, please fill out the [COVID Travel form that can be found here](https://about.gitlab.com/handbook/travel/#policy-and-guidelines) and attach to this issue in this section. 

If multiple marketers are attending this event, please ensure all COVID Travel forms have been added before seeking CMO approval. 

Upon leadership approval of this issue, there will be no other steps you need to take to seek your own approval. 

## Next Steps 
1. Assign to your manager
1. Manager to review/provide feedback 
1. Director will tag in issue VP of Integrated Marketing for approval 
1. VP of Integrated Marketing will tag in issue CMO for reivew & approval 
1. Once you've received full approval, FMM to update label to `In person event::Approved` and update the due date of this issue to be 30 days before the start of the event. 

# 30 Day check point 
- [ ] 30 days before event, confirm there has been no change in the [COVID travel exception policy](/handbook/travel/#travel-guidance-covid-19). 

/label ~"Field Marketing" ~"In person event::Requested" 
/confidential
