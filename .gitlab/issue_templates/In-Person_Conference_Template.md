## :notepad_spiral: Event Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **FMM/Event Owner:** 
* **FMC:**
* **Tactic/Event Type:** [**Conference (In-Person)**](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#conference)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Official Event Name:** 
* **Date:**
* **Location:**
* **Time Zone for Event:**  
* **Event Website:**
* **Landing Page:**
* **If we sponsored last year, link to SFDC campaign:** 
* **Allocadia ID (line item ID used for tactic charges):** (to be filled in by the FMC)
   * [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
* **Campaign Name (Marketo/SFDC):** (to be filled in by the FMC)
* **SAO Goal:**
* **Budgeted Costs (sponsorship + auxiliary cost):** 
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## Splitting Tactics
*This section is to be filled in if you are splitting your tactic between sub-regions. Please make sure each activity plan's line item panels match and are completed before submitting a contract request to your FMC.*
* **Lead FMM:** (please specify the FMM owner for SFDC here)
* **Allocadia IDs:** (please list the Allocadia category or sub-category IDs in each activity plan here for your FMC to reference)
* **Budget Breakdown:** (please list all sub-regions participating and what cost is allocated for each sub-region)

## :vertical_traffic_light: User Journey
(FMM to provide a description of the user journey)

## :paperclip: Planning & Recap Spreadsheet
Make a copy of the [Field Marketing Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1) and save here. When doing so, just delete out this entire line and leave the link back to the document itself. NOTE: Be sure the document can be edited by anyone at GitLab. 
        
## :sleuth_or_spy: Description of Event
(Description)

## :level_slider: Sponsorship Level & Details 
(Description)

## :mega: Does this event have a speaking engagement?
(If yes, please provide details here)

## :white_check_mark: FMC Checklist 
* Will leads be provided for upload? `YES/NO`
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists
* Please specify if follow up is requested, including send date and time/time zone:
  * [ ] Follow up: `Date/Time/Time Zone`
    * Do you want to send different follow up emails to different audiences? `YES/NO` (If YES, you must provide copy for the different emails in your copy doc and have specified audiences for each email send)
* Does this event have a speaking session? `YES/NO`
  * If YES, will a separate lead list be provided for the session that should be tracked separately in SFDC? `YES/NO`
  * If YES, will you require a follow up email sent to speaking session attendees? `YES/NO`
* Add to event calendar? YES
* Add to the [GitLab Events Page](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)? `YES/NO`
   * If YES please link or add appropriate event description/summary of event to be added to the events page here: `Event description`  
   
## :two_women_holding_hands: Channel Partner Section 
This section must be filled out if you are running this campaign with a channel partner. 

- [ ] Please add the name of the channel partner to the title of this issue. 
- [ ] Is the partner speaking? YES/NO    
- [ ] Is the partner driving registration? YES/NO   
- [ ] If the partner is driving registration, you must provide them with a unique UTM. 
   - [ ] Please add the unique UTM here: example: https://page.gitlab.com/pies-in-july-virtual-pizza-registration-page.html?utm_medium=email&utm_source=wwt&utm_campaign=piesinjulyfy22q2&utm_content=partnerlandingpage
   - [ ] Please add the SFDC UTM tracker: [example](https://gitlab.my.salesforce.com/00O4M000004oMS7). 
- [ ] Is the partner standing up the landing page for registration?  YES/NO   
- [ ] Is the partner supplying graphics? YES/NO  
   - [ ] If yes, have these graphics been approved by the design team? YES/NO    
- [ ] In Person Event? YES/NO
   - [ ] Are we in a Partner's Booth or Pavillion: YES/NO
      - [ ] If yes, which one? BOOTH/PAVILLION
   - [ ] Do we set up our own booth: YES/NO
   - [ ] Is our participation at a kiosk: YES/NO

### Lead follow-up with Channel Partner 
- [ ] Will the partner do initial lead follow-up? YES/NO   
   - [ ] If yes, will GitLab receive leads at any point?   
   - [ ] Please describe how the partner plans to follow-up on leads received from this event/campaign:    
- [ ] Please add any additional, relevant notes regarding the partnership on this event/campaign here: 


## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the FMM is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate the SDR issue to the epic. 

* What is the lead follow-up process? 
Will we be asking the attendees who they want to have follow up with them? Will our SDRs do the follow-up and then assign the opp to the channel? etc.

## :construction_site: Prepare
* [ ] Attendee List: Please link attendee list tab here. NOTE: This should be added as a tab on the planning and recap spreadsheet and should not be in a separate document.
* [ ] Once [these items](https://about.gitlab.com/handbook/marketing/field-marketing/#items-required-by-fmm-to-request-moving-an-issue-from-plan-to-wip) are complete, ping your FMC in a comment to request moving the issue from [Plan to WIP](https://about.gitlab.com/handbook/marketing/field-marketing/#moving-from-plan-to-wip).
* [ ]  If an [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) is needed, FMM to fill out and submit.
* [ ]  All calls scheduled or covered in regular synch meetings with attendees
   * [ ]  Kick-off call with all DRI's scheduled 6 weeks pre-event
   * [ ]  Prep call scheduled 1 week pre-event
* [ ]  Logo, company description and artwork sent to organizer - [Company description, product messaging and 
GitLab positioning](https://about.gitlab.com/handbook/marketing/product-marketing/) and [GitLab Logos](https://about.gitlab.com/press/#press-kit)
* [ ]  Press list requested and shared with GitLab PR (Highwire PR for AMER, Speakeasy for EMEA, Bench PR for APAC) 
* [ ]  Slack channel created and attendees invited
* [ ]  Tickets allocated and ordered
* [ ]  Save the Date sent for staff
* [ ]  Staff registered to event
* [ ]  All travel and hotel accomodations have been booked and added to the Planning Spreadsheet

## :moneybag: Financial
* [ ] Will this campaign include the use of Marketing Development Funds (MDFs)? More details on the MDF program can be [found here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds). 
   * [ ] YES/NO
   * [ ] If YES, please tag the Channel Marketing Manager and the Channel Account Manager (CAM) `here`
* [ ]  Spend added to Allocadia activity plan
* [ ]  Coupa requisition created by FMC - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	 

## :performing_arts: Booth/Theatre
* [ ]  Booth design issue created (if required)
* [ ]  Positioning and Messaging confirmed
* [ ]  Booth items ordered: Electric, AV, furnishings, carpet, booth buildout, etc.
* [ ]  Booth staff scheduled and added to the Planning Spreadsheet
* [ ]  Booth slide deck created
* [ ]  Click through demo set up (iPad)

#### Lead scanning
* [ ]  Lead Scanner / App ordered: `qty` `scanner number`
* [ ]  Lead license assigned: @username

## :hourglass_flowing_sand: Deliverables
*Examples below, please fill in chart with details pertaining to your event to help track deliverables.*

| Items                                     | Due Date     | Completed | 
|-------------------------------------------|--------------|-----------|
| Branding / Logos for the Virtual Booth    |              |           |
| Assets for Booth                          |              |           |
| Staff List                                |              |           |

## :busts_in_silhouette: Staffing 
Please read through the [Event Handbook page](https://about.gitlab.com/handbook/marketing/events/#employee-booth-guidelines) for best practices at events. Once you commit to an event, please make sure to plan to attend.  
   * [ ] SAL:
   * [ ] SDR:
   * [ ] SA: If you need an SA to attend, [please follow their triage process](https://about.gitlab.com/handbook/marketing/events/#requesting-technical-staffing). 
   * [ ] PMM: If you need PMM support, [please follow their triage process](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport).
   * [ ] Speaker: If you need a customer speaker, [please follow the Customer Reference Process](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event). 
   * [ ] other:  

## :airplane: Travel
Make sure you have manager approval and [COVID committee approval](https://about.gitlab.com/handbook/travel/#policy-and-guidelines) to attend
   * [ ] **Travel Dates:**
   * [ ] **Link to hotel block or recommendation (if applicable):**
   * [ ] **DEADLINE to book your travel:**
   * [ ] **Travel details added to event sheet**

## :package: Swag and Event Assets

### Shipping to Event  
* **Shipping Address:**   
* **Point of Contact:**  
* **Deadline for Shipments:**  
* **Items Ordered/Quantities/Vendor** (example: 100 notebooks from Nadel):  
* **Tracking Number:**    
* [ ] **Items Received**  

### Return Shipping
* **Onsite DRI Responsible for Return Shipping:**  
* **Nadel return shipping label provided?** YES/NO  
*   **If no:**  
	* **Address for Return:**  
	* **Fedex Account Number:** Available in marketing 1pass if needed
* **Return Tracking Number** (Provided by Onsite Event DRI):  
* [ ] **Items Shipped Back**  

## :iphone: Event Promotion
* [ ] Social issue created 
* [ ] LinkedIn campaign setup

## :checkered_flag: Post Event
* [ ]  Lead list received from organizer
* [ ]  All pictures uploaded to Google Drive
* [ ]  List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign 
* [ ]  Lead list cleaned and uploaded and add to nurture submitted
* [ ]  Follow up email triaged
* [ ]  Event recap provided by staff in the Planning Spreadsheet

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Events" ~"Field Marketing" ~"mktg-status::plan" ~"In-Person Conference"
