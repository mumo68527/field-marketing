<!-- Purpose of this issue: To request an invitation email be sent via Marketo. Please note that for multiple invitations, multiple issues must be created in order to be addressed appropriately in time-based milestones. -->

## Submitter Checklist
* [ ] Name this issue `Email Invitation <#>: <tactic/campaign name>` without including the brackets (ex. Email Invitation 1: Security Workshop)
* [ ] Due date: Set *desired* due date (dependent on this submitter checklist being completed *at least 5 business days prior*) based on [workback timeline helper](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) - [see handbook for more information about due dates and SLAs](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#turnaround-time-and-slas)
* [ ] (Must read and check box) Acknowledge that **if multiple invitations are needed**, multiple issues must be created
* Review [email best practices](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-marketing-best-practices)
* **Provide logistical details:**
   - [ ] Requested Send Date: 
   - [ ] Requested Send Time (a specific time & add a timezone - in 15 min increments): 
   - [ ] Recipient List: Please link to your completed [Request Target List](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-confirm-target-list) issue or link your [DB1 list in Marketo](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#pushing-demandbase-lists-to-marketo)
   - [ ] Reviewers/Approvers (email addresses must be provided): 
   - [ ] Is this email [operational or non-operational](https://about.gitlab.com/handbook/marketing/marketing-operations/email-management/#send-frequency)? `Operational/Non-Operational`
* **Provide email setup details:**
   - [ ] From Name: `default: GitLab Team`
   - [ ] From Email: `default: info@gitlab.com`
   - [ ] Respond-to Email: `default: info@gitlab.com`
* [ ] Provide Final Copy: `add link to final copy googledoc` - make sure the document is editable by the emails listed in the copy doc, note: copy must be final ([see handbook](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#-turnaround-time-and-slas))
* [ ] If this section (Submitter Details) is fully complete when issue created, change ~"FMM-Verticurl::blocked" label to ~"FMM-Verticurl::triage"
* [ ] Add your regional label (AMER, EMEA, or APAC) and assign to your direct manager


## Reviewer Checklist
* [ ] Triage Manager: Confirm that required details were provided and issue is ready to be worked. Once confirmed, change ~"FMM-Verticurl::triage" to ~"FMM-Verticurl::wip" and assign Verticurl Project Manager.

## Marketo work to be completed 
* [ ] Add email to Email Marketing Calendar - include link to this issue 
* [ ] Create the email in Marketo
* [ ] Send test email to the requester and designated additional reviewer/approvers in the "Reviewers/Approvers" section of Submitter Checklist above
* [ ] When test email sent, change label to `~FMM-Verticurl::review`
* [ ] Issue requester (or reviewers) provide approval, or updates in issue comment
* [ ] Set the email to send on the agreed upon date
* [ ] Comment that the issue is scheduled for deployment (including date and time)
* [ ] Copy/Paste screenshot of email in top of issue above Submitter Checklist
* [ ] Confirm that the email deployed successfully in the comments of the issue
* Close out this issue.

/label ~"FMM-Verticurl::blocked"
