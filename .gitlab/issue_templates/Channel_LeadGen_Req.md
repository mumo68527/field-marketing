This template is to be used by Channel Account Managers (CAMs) to confirm that the channel partner the CAM is asking FMM to work with has been fully vetted, onboarded, and ready for Field Marketing to engage with. All check boxes must be filled in in order for Field Marketing to begin engaging with channel partner. 

* [ ] Signed contract with partner and partner is showing as `Authorized` in the [Channel global SFDC report](https://gitlab.my.salesforce.com/00O4M000004aKef).
* [ ] Open/Select partner - Choose one and delete the other
* [ ] Partner target account list - link to SFDC list or manual list 
* [ ] Partner + GitLab value prop - link here or write 
* [ ] [Partner is fully on boarded and has been Sales Core Certified](https://about.gitlab.com/handbook/resellers/#training-and-certification-program-and-requirements)
* [ ] Assign this issue to the correct [Country Manager](https://gitlab.com/gitlab-com/marketing/field-marketing#fm-managers) for your region

### Next Steps 
Once all check marks are completed by the CAM and the country manager has been assigned, the Country FMM Manager will review and will reassign issue to specific FMM in region based avialability, type of activity, and location of targeted event/activity.

* [ ] Country manager to tag `assigned FMM` here

/label ~"Field Marketing" ~"mktg-status::plan"
