<!-- Purpose of this issue: To request reservation of a date for the Zoom license for Field Marketing-run GitLab-hosted virtual events (webcasts and workshops) dry run. -->

## Submitter Checklist
* [ ] Name this issue `Schedule Dry-Run: <name of campaign>` (ex. Schedule Dry-Run: Making the Case for CI/CD)
* [ ] Due date: Set due date based on [SLA Worksback Schedule](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#turnaround-time-and-slas)
* **Confirm Details**
  - [ ] Webcast/Workshop Date:  `(Insert Webcast Date in the following format MM-DD-YYYY)`
  - [ ] Webcast/Workshop Time:  `(Insert Webcast Time in local/Time UTC) - example: 10:00am PDT / 5:00pm UTC`
  - [ ] Final Presenters - **FMM to ping FMC when below DRIs have been finalized at least five days prior to issue due date**
    - [ ] Presenter/Lead SA:
    - [ ] Presenter 2:
    - [ ] Presenter 3 (optional):
    - [ ] Q&A Coordinator:
    - [ ] Q&A Support 1:
    - [ ] Q&A Support 2 (optional):
    - [ ] MC:
    - [ ] Host:

## Dry Run Date/Time
* [ ] `Dry Run Date/Time/Time Zone` *(To be added by the FMC once dry run has been scheduled and confirmed)*

## Reviewer Checklist
* [ ] Book dry run in GitLab-hosted Webcast calendar (date based on SLA workback schedule). Dry run should be scheduled for 60 minutes.
* [ ] Include webcast/workshop team and FMM listed above on calendar invite. 
* [ ] Include epic and dry run agenda (linked in epic) on calendar invite.
* [ ] Add any members of the webcast/workshop team to the Zoom setup if they haven't already been added 
* [ ] Add invidual Zoom links to the Dry Run/Day of Agenda doc (Zoom links for dry run will be the same links as for the webcast/workshop).
* [ ] Add the dry run date to the Timeline of Events in the Dry Run/Day of Agenda and in the section above in this issue
* [ ] Re-visit the calendar invite for the webcast/workshop and add the webcast/workshop team and FMM listed above.
* [ ] Close out this issue

<!-- DO NOT UPDATE - LABELS
/label ~"mktg-status::wip" ~"Field Marketing"
-->
