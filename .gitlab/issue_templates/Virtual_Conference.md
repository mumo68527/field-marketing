## :notepad_spiral: Event Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **FMM/Event Owner:** 
* **FMC:**
* **Tactic/Event Type:** [**Conference (Virtual)**](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#conference)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Official Event Name:** 
* **Date:**
* **Location:** Virtual
* **Time Zone for Event:**  
* **Event Website:**
* **Landing Page:**
* **If we sponsored last year, link to SFDC campaign:** 
* **Allocadia ID (line item ID used for tactic charges):** (to be filled in by the FMC)
   * [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
* **Campaign Name (Marketo/SFDC):** (to be filled in by the FMC)
* **SAO Goal:**
* **Budgeted Costs (sponsorship + auxiliary cost):** 
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## Splitting Tactics
*This section is to be filled in if you are splitting your tactic between sub-regions. Please make sure each activity plan's line item panels match and are completed before submitting a contract request to your FMC.*
* **Lead FMM:** (please specify the FMM owner for SFDC here)
* **Allocadia IDs:** (please list the Allocadia category or sub-category IDs in each activity plan here for your FMC to reference)
* **Budget Breakdown:** (please list all sub-regions participating and what cost is allocated for each sub-region)

## :sleuth_or_spy: Description of Event
(Description)

## :level_slider: Sponsorship Level & Details 
(Description)

## :vertical_traffic_light: User Journey
(FMM to provide a description of the user journey)

## :mega: Does this event have a speaking engagement?
(If yes, please provide details here)

## :paperclip: Planning & Recap Spreadsheet
Make a copy of the [Field Marketing Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1) and save here. When doing so, just delete out this entire line and leave the link back to the document itself. NOTE: Be sure the document can be edited by anyone at GitLab. 

## :white_check_mark: FMC Checklist 
* Will leads be provided for upload? `YES/NO`
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists
* Please specify if follow up is requested, including send date and time/time zone:
  * [ ] Follow up: `Date/Time/Time Zone`
    * Do you want to send different follow up emails to different audiences? `YES/NO` (If YES, you must provide copy for the different emails in your copy doc and have specified audiences for each email send)
* Does this event have a speaking session? `YES/NO`
  * If YES, will a separate lead list be provided for the session that should be tracked separately in SFDC? `YES/NO`
  * If YES, will you require a follow up email sent to speaking session attendees? `YES/NO`
* Add to event calendar? YES
* Add to the [GitLab Events Page](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents)? `YES/NO`
   * If YES please link or add appropriate event description/summary of event to be added to the events page here: `Event description`  

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the FMM is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate the SDR issue to the epic. 

## :two_women_holding_hands: Channel Partner Section 
This section must be filled out if you are running this campaign with a channel partner. 

- [ ] Please add the name of the channel partner to the title of this issue. 
- [ ] Is the partner speaking? YES/NO    
- [ ] Is the partner driving registration? YES/NO   
- [ ] If the partner is driving registration, you must provide them with a unique UTM. 
   - [ ] Please add the unique UTM here: example: https://page.gitlab.com/pies-in-july-virtual-pizza-registration-page.html?utm_medium=email&utm_source=wwt&utm_campaign=piesinjulyfy22q2&utm_content=partnerlandingpage
   - [ ] Please add the SFDC UTM tracker: [example](https://gitlab.my.salesforce.com/00O4M000004oMS7). 
- [ ] Is the partner standing up the landing page for registration?  YES/NO   
- [ ] Is the partner supplying graphics? YES/NO  
   - [ ] If yes, have these graphics been approved by the design team? YES/NO    
- [ ] In Person Event? YES/NO
   - [ ] Are we in a Partner's Booth or Pavillion: YES/NO
      - [ ] If yes, which one? BOOTH/PAVILLION
   - [ ] Do we set up our own booth: YES/NO
   - [ ] Is our participation at a kiosk: YES/NO

### Lead follow-up with Channel Partner 
- [ ] Will the partner do initial lead follow-up? YES/NO   
   - [ ] If yes, will GitLab receive leads at any point?   
   - [ ] Please describe how the partner plans to follow-up on leads received from this event/campaign:    
- [ ] Please add any additional, relevant notes regarding the partnership on this event/campaign here: 

## :construction_site: Prepare
* [ ] Attendee List: Please link attendee list tab here. NOTE: This should be added as a tab on the planning and recap spreadsheet and should not be in a separate document.
* [ ] Once [these items](https://about.gitlab.com/handbook/marketing/field-marketing/#items-required-by-fmm-to-request-moving-an-issue-from-plan-to-wip) are complete, ping your FMC in a comment to request moving the issue from [Plan to WIP](https://about.gitlab.com/handbook/marketing/field-marketing/#moving-from-plan-to-wip).
* [ ]  If an [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=social-gtm-organic) is needed, FMM to fill out and submit.
* [ ]  All calls scheduled or covered in regular synch meetings with attendees
   * [ ]  Kick-off call with all DRI's scheduled 6 weeks pre-event
   * [ ]  Prep call scheduled 1 week pre-event
* [ ]  Logo, company description and artwork sent to organizer - [Company description, product messaging and 
GitLab positioning](https://about.gitlab.com/handbook/marketing/product-marketing/) and [GitLab Logos](https://about.gitlab.com/press/#press-kit)
* [ ]  Press list requested and shared with GitLab PR (Highwire PR for AMER, Speakeasy for EMEA, Bench PR for APAC) 
* [ ]  Slack channel created and attendees invited
* [ ]  Tickets allocated and ordered
* [ ]  Save the Date sent for staff
* [ ]  Staff registered to event

## :moneybag: Financial
* [ ] Will this campaign include the use of Marketing Development Funds (MDFs)? More details on the MDF program can be [found here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds). 
   * [ ] YES/NO
   * [ ] If YES, please tag the Channel Marketing Manager and the Channel Account Manager (CAM) `here`
* [ ]  Spend added to Allocadia activity plan
* [ ]  Coupa requisition created by FMC - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	 

## :memo: Virtual Booth Tips and Tricks
* [Example Best Practices Doc](https://docs.google.com/document/d/1FOS-W36lezJ7VMYKbas6LIBQFR3wSTyo_zDu1t6ebw8/edit) 
* [Example PDF of How to Navigate a Portal](/uploads/1144d89f1bac3c5c4707f78b51d726e1/BoothRep-General.pdf)
* [Example of How to Navigate a Portal Video](https://drive.google.com/file/d/18dZGpGCP8YceA24kI6Cxiwxv30yezPnN/view)

## :pushpin: Booth Assets
Examples below. If you have new assets you would like to utilize in your follow-up email, please coordinate with your MPM directly as you might need to open a [Pathfactory Request](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=pathfactory_request).

### General Booth Assets
For more assets, review the [Resources Spreadsheet](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=722919699), [Resources Page](https://about.gitlab.com/resources/) and [Design Studio Dashboard of Assets](https://datastudio.google.com/u/0/reporting/bb7a37e5-d63e-421a-9b3b-8d1ec80f72dd/page/OjYYB).

- [ ] [What is GitLab?](https://www.youtube.com/watch?v=-_CDU6NFw7U)
- [ ] [GitLab Infomercial Video](https://www.youtube.com/watch?v=gzYTZhJlHoI)  
- [ ] [Ebook - Scaled-CI-CD](/uploads/943e0e01dbcce9f014da1189997575a3/gitlab-scaled-ci-cd-.pdf) 
- [ ] [Ebook - Remote Playbook](/uploads/34642ae8653ed51280dde1a9c7385add/remote-playbook.pdf)
- [ ] [Whitepaper - Reduce Cycle Time to Deliver Value](/uploads/ede3b3e00db838518bd29845ba9a7507/source_resources_downloads_201906-whitepaper-reduce-cycle-time.pdf) 
- [ ] [The Forrester Wave - Cloud-Native Continuous Integration Tools](/uploads/9ad85130be17d9ced05647283d738f40/The_Forrester_Wave___Cloud-Native_Continuous_Integration_Tools__Q3_2019.pdf)
- [ ] [Ebook - 2020 DevSecOps Report](/uploads/cc655c78ad1a23ce8ade736c4d3c2311/2020-devsecops-report.pdf)    
- [ ] [Video - Making the case for CI/CD in your organization](https://www.youtube.com/watch?v=MqxcGV9NT8k&feature=emb_logo)   
- [ ] [Use Case - NVIDIA - How GitLab Geo supports NVIDIA's innovation](https://about.gitlab.com/customers/Nvidia/)
- [ ] [Solution Brief - GitLab on AWS](/uploads/74879d7fa7e1919948d9674d6e4be315/source_resources_downloads_GitLab_AWS_Solution_Brief.pdf) 
- [ ] [Solution Brief - GitLab on GCP](/uploads/d830cb6d747573e252c5a6520884010a/source_resources_downloads_GitLab_GCP_Solution_Brief.pdf) 
- [ ] [A Seismic Shift in Application Security whitepaper](/uploads/fd88f354f9f25bd5a73dab555b26a1dc/gitlab-seismic-shift-in-application-security-whitepaper.pdf)
- [ ] [DevSecOps Solutions Page](https://about.gitlab.com/solutions/dev-sec-ops/) 
- [ ] [GitLab — A single application for your entire software development lifecycle](https://www.youtube.com/watch?v=yjxrBSllNGo)
- [ ] [The Benefits of a Single DevOps Platform](https://www.youtube.com/watch?v=MNxkyLrA5Aw)

### PubSec Booth Assets
For more information, visit the [Public Sector Go to Market page](https://about.gitlab.com/handbook/marketing/product-marketing/public-sector-gtm/).

- [ ] [GitLab’s Hardened Container Image for Secure Software Development](https://docs.google.com/document/d/1Rl0050Z-PYogJAraHNdLPvgpwAmqlJ7-81PpCMpw_Fw/edit#)
- [ ] [PubSec Capabilities Statement](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)
- [ ] [Speed to Mission (whitepaper)](https://about.gitlab.com/resources/whitepaper-speed-to-mission/)
- [ ] [The Hidden Costs of DevOps Toolchains (with guest speaker from Forrester)](https://about.gitlab.com/webcast/simplify-to-accelerate/)
- [ ] [DevOps Powering Your Speed to Mission (webcast)](https://about.gitlab.com/webcast/devops-speed-to-mission/)
- [ ] [Mission Mobility: A DevSecOps Discussion (webcast)](url)
- [ ] [Government Matters Technology Leaders Innovation Series: Low-to-High Collaboration (with Marc Kriz) (video)](https://www.youtube.com/watch?v=RvZSTCbPkiE&feature=youtu.be)
- [ ] [Modernizing Government IT through DevOps (whitepaper)](https://about.gitlab.com/resources/whitepaper-modernizing-government-it/)
- [ ] [Cross Domain DevSecOps: Low-to-High Side Collaboration](https://drive.google.com/file/d/1ewgLvmlfg3f0CVE3B6mu9wRtgK-TcORz/view)
- [ ] [DevSecOps: How Proactive Security Integration Reduces Your Agency's Risks & Vulnerability](https://gcn.com/whitepapers/2020/06/gitlab-devsecops-wp-060120.aspx?&pc=G0136GCN&utm_source=webmktg&utm_medium=E-Mail&utm_campaign=G0136GCN)
- [ ] [Hardened Containers: The Right Application Platform Can Help DoD Develop Its DevSecOps Culture](https://drive.google.com/file/d/1cszlj5fKFE12qFd4aMNij0a83v-3Fe4r/view)
- [ ] [VPAT Template](https://docs.google.com/document/d/1o6gCe2Se_Cuia-VsrUTdGvESNZVefyoqFAu1NY8rJpQ/edit)
- [ ] [What is GitLab? Speed. Efficiency. Trust](https://www.youtube.com/watch?v=-_CDU6NFw7U&list=PL05JrBw4t0KpEQDAhM3Txia7QHdYlngMa&index=2&t=16s)
- [ ] [A Beginner's Guide to GitOps](https://learn.gitlab.com/c/beginner-guide-gitops?x=H2TMk5&utm_medium=email&utm_source=marketo&utm_campaign=iacgitops&utm_content=beginnerguidegitops&mkt_tok=MTk0LVZWQy0yMjEAAAF9ckzHYSGuak8zU2ggbz7gnOo0sVhN_o8TLhWOK4LGEYUzEUPgYs9EvVR92cWIchbqK1S2NsgbnQ9or5uaDNBSK3wajpvbvGpG7DAGvnvaFK2HzA)


## :hourglass_flowing_sand: Deliverables
*Examples below, please fill in chart with details pertaining to your event to help track deliverables.*

| Items                                     | Due Date     | Completed | 
|-------------------------------------------|--------------|-----------|
| Branding / Logos for the Virtual Booth    |              |           |
| Assets for Virtual Booth                  |              |           |
| Staff List                                |              |           |
| Presenter Name / Info (Bio, Abstract, etc)|              |           |
| Presentation Title + Abstract             |              |           | 
| Select Pre-Record Time Slot               |              |           | 		

## :busts_in_silhouette: Staffing 
Once you commit to an event, please make sure to plan to attend.  
   * [ ] SAL:
   * [ ] SDR:
   * [ ] SA: If you need an SA to attend, [please follow their triage process](https://about.gitlab.com/handbook/marketing/events/#requesting-technical-staffing). 
   * [ ] PMM: If you need PMM support, [please follow their triage process](https://about.gitlab.com/handbook/marketing/product-marketing/#requesting-strategic-marketing-team-helpsupport).
   * [ ] Speaker: If you need a customer speaker, [please follow the Customer Reference Process](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-customer-to-speak-andor-otherwise-support-an-event). 
   * [ ] other:  

## :package: Swag and Event Assets
List out swag details here and tag the FMC if applicable.

## :iphone: Event Promotion
* [ ] Social issue created (FMC to create when setting up epic and sub-issues) - [FMC to link social issue here]
* [ ] LinkedIn campaign setup

## :checkered_flag: Post Event
* [ ]  Lead list received from organizer
* [ ]  Follow up email
* [ ]  Lead cleaned up (24 hours after event close) and shared in the MktgOps issue
* [ ]  List locked (all changes after lock to be made in SFDC) and leads uploaded to SFDC campaign 
* [ ]  Event recap & feedback

## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Events" ~"Field Marketing" ~"mktg-status::plan" ~"Virtual Conference"
