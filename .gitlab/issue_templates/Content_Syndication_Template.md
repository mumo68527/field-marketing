## :notepad_spiral: Campaign Details 
*Please do not delete anything in this section. Fill out information in full and complete your budget line item before moving to WIP.*
* **FMM/Event Owner:** 
* **FMC:**
* **Campaign Type:** [**Content Syndication**](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/)
* **GTM Motion:** Select the [motion](https://about.gitlab.com/handbook/marketing/plan-fy22/#gtm-motions) that applies towards this tactic/event 
* **Type of Content:** (Whitepaper, eBook, Report, Video, or General) 
* **Content Title:** 
* **Launch Date of Content:**  
* **End Date of Content:** (estimated end date of campaign)
* **Allocadia ID (line item ID used for tactic charges):** (to be filled in by the FMC)
   * [ ] Allocadia ID created in Netsuite (to be checked off by FMC once ID has been submitted in [this spreadsheet](https://docs.google.com/spreadsheets/d/1wBqvCvmou4afnb0p8lBXVvFmsl-j0IehS7GdfJybGzg/edit#gid=518252060))
* **Campaign Name (Marketo/SFDC):** (to be filled in by the FMC)
* **SAO Goal:** 
* **Budgeted Cost:** 
* **Budget Holder:** Field Marketing
* **Region (APAC/AMER/EMEA):**  
* **Sub-Region (East/West/PubSec or Southern/Northern/UKI/DACH):** 
* **Sales Segment (large, mid-market or SMB):**
* **Sales Territory (if specific):** 

## Splitting Tactics
*This section is to be filled in if you are splitting your tactic between sub-regions. Please make sure each activity plan's line item panels match and are completed before submitting a contract request to your FMC.*
* **Lead FMM:** (please specify the FMM owner for SFDC here)
* **Allocadia IDs:** (please list the Allocadia category or sub-category IDs in each activity plan here for your FMC to reference)
* **Budget Breakdown:** (please list all sub-regions participating and what cost is allocated for each sub-region)

## :vertical_traffic_light: User Journey
(FMM to provide a description of the user journey)

## :paperclip: Planning & Recap Spreadsheet 
Make a copy of the [Field Marketing Template](https://docs.google.com/spreadsheets/d/1i2-CdlsvW2x98NvJJ1mLcVq6ymehaRqSu2ckkWiV5ko/edit#gid=812678489&range=A1) and save here. When doing so, just delete out this entire line and leave the link back to the document itself. NOTE: Be sure the document can be edited by anyone at GitLab.   

## :white_check_mark: FMC Checklist 
* Will leads be provided for upload? `YES/NO`
  * If YES, please indicate if you will only have a single lead list for upload, or if you will have multiple lead lists (if receiving leads from a 3rd party vendor over time):
      - [ ] Single Lead List
      - [ ] Multiple Lead Lists

## :calendar: Deadlines 
* [ ]

## :trophy:  Outreach and Follow-Up (ISR/SDR)
If ISR/SDR support is being requested, the FMM is to open an [ISR/SDR Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=ISR_SDR_FMTemplate) and link the issue in this section here, as well as associate to the SDR issue to the epic. 

## :construction_site: Prepare
* [ ] Once [these items](https://about.gitlab.com/handbook/marketing/field-marketing/#items-required-by-fmm-to-request-moving-an-issue-from-plan-to-wip) are complete, ping your FMC in a comment to request moving the issue from [Plan to WIP](https://about.gitlab.com/handbook/marketing/field-marketing/#moving-from-plan-to-wip).

## :moneybag: Financial
* [ ] Will this campaign include the use of Marketing Development Funds (MDFs)? More details on the MDF program can be [found here](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds). 
   * [ ] YES/NO
   * [ ] If YES, please tag the Channel Marketing Manager and the Channel Account Manager (CAM) `here`
* [ ]  Spend added to Allocadia activity plan
* [ ]  Coupa requisition created by FMC - `Link Coupa req here`
* [ ]  Contract fully executed
* [ ]  Invoice received
* [ ]  Invoice paid    	  	


## :open_file_folder: Assignments and Labels

* `Assign yourself to the issue`
* `Assign the Quarter and Region tags to the issue`

/label ~"Content Syndication" ~"Field Marketing" ~"mktg-status::plan" 
